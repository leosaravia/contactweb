namespace ContactWeb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactWeb.Models.ContactWebContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "ContactWeb.Models.ContactWebContext";
        }

        protected override void Seed(ContactWeb.Models.ContactWebContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //    context.Contacts.AddOrUpdate(

            //        new Models.Contact { FirstName = "Leonardo", LastName = "Saravia", City = "Montevideo", Birth = Convert.ToDateTime("19/08/1983"), Email = "leo.saravia01@gmail.com", Phone = "1234" },
            //        new Models.Contact { FirstName = "Contact1", LastName = "Contact1Last", City = "Montevideo", Birth = Convert.ToDateTime("19/08/1983"), Email = "Contact.mail01@gmail.com", Phone = "12345" }
            //        );
            //}

            context.Contacts.AddOrUpdate(
                p => p.Id,
                new Models.Contact
                {
                    Id = 1,
                    Birth = new DateTime(1920, 01, 20),
                    City = "Chicago"
                                    ,
                    Email = "doc.mccoy@starfleet.com",
                    FirstName = "DeForest",
                    LastName = "Kelley"
                                    ,
                    Phone = "123-456-7890",
                    State = "IL"
                                    ,
                    StreetAdddress = "Sickbay"
                                    ,
                    UserId = new Guid("56f5dd2c-13b1-436b-a5a8-92400ca83761")
                                    ,
                    Zip = "98765"
                }
                , new Models.Contact
                {
                    Id = 2,
                    Birth = new DateTime(1920, 03, 03),
                    City = "New York"
                                    ,
                    Email = "i.beam.you.up@starfleet.com",
                    FirstName = "James",
                    LastName = "Doohan"
                                    ,
                    Phone = "345-678-9012",
                    State = "NY"
                                    ,
                    StreetAdddress = "Engineering"
                                    ,
                    UserId = new Guid("56f5dd2c-13b1-436b-a5a8-92400ca83761")
                                    ,
                    Zip = "87654"
                }
                , new Models.Contact
                {
                    Id = 3,
                    Birth = new DateTime(1931, 03, 26),
                    City = "Los Angeles"
                                    ,
                    Email = "its.only.logic@starfleet.com",
                    FirstName = "Leonard",
                    LastName = "Nimoy"
                                    ,
                    Phone = "987-654-3210",
                    State = "CA"
                                    ,
                    StreetAdddress = "Science Station 1"
                                    ,
                    UserId = new Guid("a26a9de7-e2f0-4bea-ba46-37deeaaafd66")
                                    ,
                    Zip = "76543-2109"
                }
                , new Models.Contact
                {
                    Id = 4,
                    Birth = new DateTime(1931, 03, 22),
                    City = "Riverside"
                                    ,
                    Email = "the.captain@starfleet.com",
                    FirstName = "William",
                    LastName = "Shatner"
                                    ,
                    Phone = "765-432-1098",

                    State = "IA"
                                    ,
                    StreetAdddress = "The Bridge"
                                    ,
                    UserId = new Guid("a26a9de7-e2f0-4bea-ba46-37deeaaafd66")
                                    ,
                    Zip = "65432-0123"
                }
            );
        }
    }
}
